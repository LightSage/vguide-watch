# vGuide Watch - A Script to Send Updates on New YouTube Channel Uploads
# Copyright (C) 2019 LightSage
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import feedparser
import requests
import config
import time
import json
from datetime import datetime

data = json.load(open("data.json", "r"))
for f in config.channel_ids:
    feed = feedparser.parse(f'https://www.youtube.com/feeds/videos.xml?channel_id={f}')
    for entry in feed["entries"]:
        # print(entry)
        # Grab some things
        vguide_title = entry["title"]
        vguide_link = entry["link"]
        vguide_channel = entry["author"]
        vgc = str(vguide_channel)

        pub_time = time.mktime(entry["published_parsed"])
        # Take published time and make it readable
        readable_time = datetime.utcfromtimestamp(pub_time).strftime(config.time_format)
        if vgc not in data:
            data[vgc] = {"lastupdate": 0}
        if pub_time <= data[vgc]["lastupdate"]:
            continue

        webhook_msg = f"🚨 **Video Guide Detected:**\n__Channel Name:__"\
                      f" {vguide_channel}\n"\
                      f"__Title:__ {vguide_title}\n"\
                      f"__Published:__ {readable_time} UTC\n"\
                      f"__Link:__ {vguide_link}"

        data[vgc]["lastupdate"] = pub_time

        print(webhook_msg)

        for hook in config.webhook_urls:
            requests.post(hook, data={"content": webhook_msg})

    # Dump our contents to our json file
    json.dump(data, open("data.json", "w"))
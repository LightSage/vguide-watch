# vGuide Watch

A script that sends updates on new uploads from YouTube Channels to Discord Webhooks.


## How To Use
- Install Python3.6+
- Install the requirements in `requirements.txt`
- Copy `config.py.example` and rename it to `config.py`
- Configure `config.py`
- Run the script and run it periodically with systemd timers, cron, or whatever.


## License
GPLv3
```
# vGuide Watch - A Script to Send Updates on New YouTube Channel Uploads
# Copyright (C) 2019 LightSage
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
```